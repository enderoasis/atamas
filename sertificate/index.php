<?php
	session_start();
  	header('Content-type: image/jpeg');
  	require_once '../vendor/autoload.php';
	use \RedBeanPHP\R as R;
	$dotenv = \Dotenv\Dotenv::create( $_SERVER['DOCUMENT_ROOT'] );
	$dotenv->load();
	R::setup( 'mysql:host='.getenv("DB_HOST").';dbname='.getenv("DB_NAME") , getenv("DB_USER"), getenv("DB_PASS"), false );
	if (empty($_SESSION['logged_user'])) {
		header( 'Refresh: 0; url=../login.php' );
	} else {
		
		// $date_end = R::findOrCreate('users', ' email = ? ', array($user_email));
		// $book = R::findOrCreate( 'users', [
  //       'email' => $user_email, 
  //       'cert_gen' => ] );

		// Create Image From Existing File
		$jpg_image = imagecreatefromjpeg('./images/original.jpg');

		// Allocate A Color For The Text
		$white = imagecolorallocate($jpg_image, 255, 255, 255);
		$black = imagecolorallocate($jpg_image, 0,0,0);
	  	$font = dirname(__FILE__) . '/fonts/calibri.ttf';

		$user_email = $_SESSION['email'];

		$user = R::findOne('users', ' email = ? ', array($user_email));
		// Set Text to Be Printed On Image
		$name = $user->name;
		$surname = $user->surname;
		$fullname = $name .' '. $surname;
		//$fullname = "Кудайбергенова Карлыгаш";
		$date = date("d.m.y"); ;

		// Print Text On Image
		imagettftext($jpg_image, 150, 0, 220, 1900, $black, $font, $fullname);
		imagettftext($jpg_image, 75, 90, 3350, 700, $black, $font, $date);

		$cert_name = getenv("APP_NAME")." - ".$fullname.".jpg";
		

		// Send Image to Browser
		$path = $_SERVER['DOCUMENT_ROOT'] . '/' . 'users'. '/';
		imagejpeg($jpg_image, $path.$cert_name);
		imagejpeg($jpg_image);

		// Clear Memory
		imagedestroy($jpg_image);
	}

  