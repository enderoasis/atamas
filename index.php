<?php
  session_start();
  require 'db.php';
  require 'vendor/autoload.php';

  $dotenv = Dotenv\Dotenv::create(__DIR__);
  $dotenv->load();

  $title = getenv('APP_NAME');

  $data1 = $_POST;
  $_SESSION['refid'] = $_GET['ref'];

 
?> 
<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$title?></title>
  	<link rel="icon" href="https://partners.saico.pro/favicon.png">
    <link rel="stylesheet" href="vendors/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="vendors/themify-icons/themify-icons.css">
    <link rel="stylesheet" href="vendors/flat-icon/flaticon.css">
    <link rel="stylesheet" href="vendors/nice-select/nice-select.css">
    <link rel="stylesheet" href="vendors/Magnific-Popup/magnific-popup.css">
    <!-- @import url("https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700"); -->
    <link rel="stylesheet" href="css/style.css">
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
   

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109692965-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-109692965-2');
</script>
  
</head>
  <body>


    <!-- ================ start header Area ================= -->
    <header class="header_area sticky-header">
		  <div class="main_menu">
        <nav class="navbar navbar-expand-lg navbar-light main_box">
				  <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					 aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						  <span class="icon-bar"></span>
						  <span class="icon-bar"></span>
						  <span class="icon-bar"></span>
				    </button>
				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
						  <ul class="nav navbar-nav menu_nav ml-auto">
						    <li class="nav-item"><a class="nav-link" href="#info">О курсе</a></li>
							  <li class="nav-item"><a class="nav-link" href="blog.php">Видеокурсы</a></li>
                <!--<li class="nav-item active"><a class="nav-link" href="#gallery">Галерея</a></li>-->
                <li class="nav-item"><a class="nav-link" href="public_offer.php">Публичная оферта</a></li>
                <li class="nav-item"><a class="nav-link" href="purchase.php">Приобрести курс </a></li>
		            <!--<li class="nav-item"><a class="nav-link" href="contact.html">Контакты</a></li>-->
                <?php if (!isset($_SESSION['reg']) && !isset($_SESSION['logged_user'] )) { ?>
                  <li class="nav-item"><a class="nav-link" href="login.php">Вход</a></li>
                <?php } ?>
                <?php if (!isset($_SESSION['reg']) && !isset($_SESSION['logged_user'] )) { ?>
                  <li class="nav-item"><a class="nav-link" href="register.php">Регистрация</a></li>
                <?php } ?>
                <?php if (isset($_SESSION['logged_user'] )) { ?>
                <li class="nav-item"><a class="nav-link" href="logout.php">Выход</a></li>
                <?php } ?>
						  </ul>
            </div>
				  </div>
        </nav>
      </div>
    </header>
  <!-- ================ end header Area ================= -->

  <main class="site-main">

    <!-- ================ start hero banner Area ================= -->
   
    <!-- ================ end hero banner Area ================= -->


    <!-- ================ start about section ================= -->
    <section class="hero-banner" id="hmain">
      <div class="container">
        <div class="row" style="bac" >
          <div class="col-md-6">
            <div class="hero-banner__content" style="margin-top: 50px;">
            <center><span style="font-size: 28px;"><center><b>Первая онлайн-школа в Казахстане</b></center></span></center>
            <center><span style="font-size: 24px;">от ведущего визажиста</span></center>
            <br>
            <center><h1>Владимира Атамаса</h1></center><br><br>
           
              <a href="/pays/pay.php?cost=30000" class="button button-hero" id="b2" >Купить видеокурс</a>
              <a href="#info" class="button button-hero" id="b1" >Об авторском курсе</a>

            </div>
     
          </div>

          <div class="col-md-6 pl-xl-4">
             <img class="img-fluid" src="img/banner/image2.jpg" alt="">
          </div>
        </div>
      </div>
    </section>
    <section class="hero-banner" id="hero">
      <div class="container">
        <div class="row" style="bac" >
          <div class="col-md-6">
            <div class="hero-banner__content" style="margin-top: 50px;">
          <div class="moba"  <center><span style="font-size: 28px;"><center><b>Первая онлайн-школа в Казахстане</b></center></span></center>
            <center><span style="font-size: 24px;">от ведущего визажиста</span></center>
            <br><br>
            <center><h1>Владимира Атамаса</h1></center><br><br>
          </div>
              <img class="img-fluid" src="img/banner/image2.jpg" alt="">
            </div>
     
          </div>

          <div class="col-md-6 pl-xl-4" id="ots1">
          <div class="moba2"><center><span style="font-size: 28px;"><center><b>Первая онлайн-школа в Казахстане</b></center></span></center>
            <center><span style="font-size: 24px;">от ведущего визажиста</span></center>
            <br><br>
            <center><h1>Владимира Атамаса</h1></center><br><br>
                </div>
              <a href="/pays/pay.php?cost=30000" class="button button-hero" id="b2" >Купить видеокурс</a>
              <a href="#about" class="button button-hero" id="b1" >Об авторском курсе</a>          </div>
        </div>
      </div>
    </section>
    <section class="section-margin about-wrapper" style="background-color: #ffff; >
      <div class="container">
        <div class="row" id="r1">
          <div class="col-lg-5 mb-5 mb-lg-0">
          <div style="position:relative; overflow:hidden; padding-bottom:56.25%" id="v1"> <iframe src="https://cdn.jwplayer.com/players/k7YKJXGi-5HCh7NEL.html" width="100%" height="100%" frameborder="0" scrolling="auto" title="B" style="position:absolute;" allowfullscreen></iframe> </div>
            <div class="about__content" id="about">
              
            
              </div></div>

          <div class="col-lg-5 offset-lg-2">
             <div class="section-intro">
                <h4 class="section-intro__title">Об обучающем курсе</h4>
                <h2 class="section-intro__subtitle">Визаж - искусство</h2>
              </div>
              <p>Видеокурс состоит из 5 видеоуроков. Каждый урок длится около 45 минут.<br><br>1)Wedding Angel / Свадебный образ<br>2)Victoria Secret / Нежный девичьий макияж<br>3)Hollywood Glam/Образ Кинодивы<br>4)Matte Drama/Пронзительный Глубокий Макияж<br>5)Fashion Obsession / Буйство Акцентов</p>
<br>
                <a href="/pays/pay.php?cost=30000" class="button button-hero" id="b3" >Купить курс</a>
            
          </div>
        </div>
      </div>
    </section>
    <section class="section-padding--large bg-soapstone" id="gallery">
      <div class="container">
        <div class="section-intro pb-70px">
          <h4 class="section-intro__title">Галерея</h4>
          <h2 class="section-intro__subtitle">Увидевший прекрасное —
 <span class="d-block">соучастник его создания</span></h2>
        </div>

        <div class="row">
 <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
            <div class="card card-team">
              <div class="card-team__img">
                <img class="card-img rounded-0" src="https://res.cloudinary.com/du8h2uiqi/image/upload/v1567882877/team5_zclh7g.jpg" alt="визаж,визаж алматы,видеокурсы,atamas,атамас">
              </div>
              <div class="card-team__position">
                <div class="card-footer d-flex justify-content-between align-items-center">
                  <div class="card-team__bio">
                                      <h4><a href="#">1.Wedding Angel / Свадебный образ</a></h4>
                                      <p></p>
                                    </div>
                </div>
              </div>
            </div>
          </div>
           <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
            <div class="card card-team">
              <div class="card-team__img">
                <img class="card-img rounded-0" src="https://res.cloudinary.com/du8h2uiqi/image/upload/v1568819911/DSC09843-min_p35d0x.jpg" alt="визаж,визаж алматы,видеокурсы,atamas,атамас">
              </div>
              <div class="card-team__position">
                <div class="card-footer d-flex justify-content-between align-items-center">
                  <div class="card-team__bio">
                                      <h4><a href="#">2.Victoria Secret / Нежный девичий макияж</a></h4>
                                      <p></p>
                                    </div>
                </div>
              </div>
            </div>
          </div>
           
           <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
            <div class="card card-team">
              <div class="card-team__img">
                <img class="card-img rounded-0" src="https://res.cloudinary.com/du8h2uiqi/image/upload/v1568819885/IMG_5490-min_huvgv8.jpg" alt="визаж,визаж алматы,видеокурсы,atamas,атамас">
              </div>
              <div class="card-team__position">
                <div class="card-footer d-flex justify-content-between align-items-center">
                  <div class="card-team__bio">
                                      <h4><a href="#">3.Hollywood Glam/<br>Образ Кинодивы</a></h4>
                                      <p></p>
                                    </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
            <div class="card card-team">
              <div class="card-team__img">
                <img class="card-img rounded-0" src="https://res.cloudinary.com/du8h2uiqi/image/upload/v1567882866/team4_agu8xv.jpg" alt="визаж,визаж алматы,видеокурсы,atamas,атамас">
              </div>
              <div class="card-team__position">
                <div class="card-footer d-flex justify-content-between align-items-center">
                  <div class="card-team__bio">
                                      <h4><a href="#">4.Matte Drama/Пронзительный Глубокий Макияж</a></h4>
                                      <p></p>
                                    </div>
                </div>
              </div>
            </div>
          </div>
         
          <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
            <div class="card card-team">
              <div class="card-team__img">
                <img class="card-img rounded-0" src="https://res.cloudinary.com/du8h2uiqi/image/upload/v1567882888/team6_dxq8hu.jpg" alt="визаж,визаж алматы,видеокурсы,atamas,атамас">
              </div>
              <div class="card-team__position">
                <div class="card-footer d-flex justify-content-between align-items-center">
                  <div class="card-team__bio">
                                      <h4><a href="#">5.Fashion Obsession / <br> Буйство Акцентов</a></h4>
                                      <p></p>
                                    </div>
                </div>
              </div>
            </div>
          </div>
         
        </div>
      </div>
    </section>
    <section class="section-margin about-wrapper" id="s1" style="background-color: #ffcdd2; margin-top: -230px;" >
      <div class="container">
        <div class="row">
          <div class="col-lg-5 mb-5 mb-lg-0" > 
            <div class="about__content" id="about2">
               <div class="section-intro">
               
                <h2 class="section-intro__subtitle">&nbsp;&nbsp;1. Wedding Angel / Свадебный образ</h2>
              </div>
             
                
              </div></div>

          <div class="col-lg-5 offset-lg-2" >
            <img class="card-img rounded-0" src="https://res.cloudinary.com/du8h2uiqi/image/upload/v1567882877/team5_zclh7g.jpg" alt="визаж,визаж алматы,видеокурсы,atamas,атамас">
            
            
          </div>
        </div>
      </div>
    </section><section class="section-margin about-wrapper" id="s2" style="background-color: #ffff;    margin-top: -230px;">
      <div class="container">
        <div class="row">
          <div class="col-lg-5 mb-5 mb-lg-0">
           <img class="card-img rounded-0" src="https://res.cloudinary.com/du8h2uiqi/image/upload/v1568819911/DSC09843-min_p35d0x.jpg" alt="визаж,визаж алматы,видеокурсы,atamas,атамас">
            <div class="about__content" id="about">
              
            
              </div></div>

          <div class="col-lg-5 offset-lg-2">
             <div class="section-intro">
               
                <br><br><h2 class="section-intro__subtitle">&nbsp;&nbsp;2. Victoria Secret / Нежный девичьий макияж</h2>
              </div>
             
            
          </div>
        </div>
      </div>
    </section>  <section class="section-margin about-wrapper" id="s3" style="background-color: #ffcdd2;    margin-top: -230px;">
      <div class="container">
        <div class="row">
          <div class="col-lg-5 mb-5 mb-lg-0" > 
            <div class="about__content" id="about2">
               <div class="section-intro">
               
                <h2 class="section-intro__subtitle">&nbsp;3. Hollywood Glam/ &nbsp;&nbsp;Образ Кинодивы</h2>
              </div>
             
                
              </div></div>

          <div class="col-lg-5 offset-lg-2" >
            <img class="card-img rounded-0" src="https://res.cloudinary.com/du8h2uiqi/image/upload/v1568819885/IMG_5490-min_huvgv8.jpg" alt="визаж,визаж алматы,видеокурсы,atamas,атамас">
            
            
          </div>
        </div>
      </div>
    </section><section class="section-margin about-wrapper" id="s4" style="background-color: #ffff; margin-top: -230px;">
      <div class="container">
        <div class="row">
          <div class="col-lg-5 mb-5 mb-lg-0">
           <img class="card-img rounded-0" src="https://res.cloudinary.com/du8h2uiqi/image/upload/v1567882866/team4_agu8xv.jpg" alt="визаж,визаж алматы,видеокурсы,atamas,атамас">
            <div class="about__content" id="about">
              
            
              </div></div>

          <div class="col-lg-5 offset-lg-2">
             <div class="section-intro">
               
             <br><br>    <h2 class="section-intro__subtitle">&nbsp;4. Matte Drama/Пронзительный Глубокий Макияж</h2>
              </div>
             
            
          </div>
        </div>
      </div>
    </section><section class="section-margin about-wrapper" id="s5" style="background-color: #ffcdd2;    margin-top: -230px;">
      <div class="container">
        <div class="row">
          <div class="col-lg-5 mb-5 mb-lg-0" > 
            <div class="about__content" id="about2">
               <div class="section-intro">
               
                <h2 class="section-intro__subtitle">&nbsp;5. Fashion Obsession/Буйство Акцентов</h2>
              </div>
             
                
              </div></div>

          <div class="col-lg-5 offset-lg-2" >
            <img class="card-img rounded-0" src="https://res.cloudinary.com/du8h2uiqi/image/upload/v1567882888/team6_dxq8hu.jpg" alt="визаж,визаж алматы,видеокурсы,atamas,атамас">
            
            
          </div>
        </div>
      </div>
    </section><section class="section-margin--large pt-xl-5" id="info">
    <div class="container">
      <div class="section-intro pb-70px">
        <h4 class="section-intro__title">Преимущества</h4>
        <h2 class="section-intro__subtitle">&nbsp;В чём <span class="d-block">уникальность курса?</span></h2>
      </div>

      <div class="row gutters-48">
        <div class="col-md-6 col-xl-3 mb-5 mb-xl-0">
          <div class="card text-center card-feature border-style">
            <div class="card-feature__icon"><img src="img/online-course.png"></div>
            <h3 class="card-feature__title">Обучение онлайн</h3><br>
            <p>Теперь нет необходимости приезжать в Алмату, тратить своё время и обучаться в группе с другими людьми. Смотрите онлайн-курс с любого устройства и в любой точке мира.</p>
          </div>
                </div>
                
                <div class="col-md-6 col-xl-3 mb-5 mb-xl-0">
          <div class="card text-center card-feature border-style">
            <div class="card-feature__icon"><img src="img/video-camera.png"></div>
            <h3 class="card-feature__title">Качество видео FULLHD</h3><br>
            <p>Видеокурс снят с 3-х
ракурсов, благодаря этому
у вас будет возможность
увидеть все детали.</p>
          </div>
                </div>
                
                <div class="col-md-6 col-xl-3 mb-5 mb-xl-0">
          <div class="card text-center card-feature border-style">
            <div class="card-feature__icon"><img src="img/sunset.png"></div>
            <h3 class="card-feature__title">Возможность выиграть поездку на о. Хайнань</h3><br>
            <p>При покупке курса , вы можете стать участником розыгрыша билетов на о.Хайнань.</p>
          </div>
                </div>
                
                <div class="col-md-6 col-xl-3 mb-5 mb-xl-0">
                <div class="card text-center card-feature border-style" >
            <div class="card-feature__icon"><img src="img/winner.png"></div>
            <h3 class="card-feature__title">Сертификат</h3><br>
            <p>Вы получите авторский
сертификат, подтверждающий ваше обучение в школе Make Up House.</p>

          </div>
        </div>
       
          

      </div>        

    </div>
  </section><section class="section-margin about-wrapper" style="background-color: #ffff;margin-top: -10%;">
      <div class="container">
        <div class="row">
          <div class="col-lg-5 mb-5 mb-lg-0">
           <img class="card-img rounded-0" src="img/banner/image1.jpg" alt="визаж,визаж алматы,видеокурсы,atamas,атамас">
            <div class="about__content" id="about">
              
            
              </div></div>

          <div class="col-lg-5 offset-lg-2">
             <div class="section-intro">
                <h3 class="section-intro__title">О преподавателе</h3>
                <h2 class="section-intro__subtitle" style="background-color: #ffcdd2;">Владимир Атамас</h2>
           <br>     <p>Основатель школы и ведущий визажист Казахстана. Проходил обучение у таких мировых преподавателей, как Ольга Томина, Сердар Камбаров, Елена Крыгина, Евгения Ежова, Валерия Пиминова. Работал для глянцевых журналов SNC, Boulevard, L'Officiel, ELLE Kazakhstan, Cosmopolitan, Harper's Bazaar и другие.  Сотрудничество: группа Виагра, группа Серебро, Ксения Собчак, Луина, Жанар Дугалова, Диана Коркунова, Ерке Есмахан, Камшат Жолдыбаева, Нагимуша, Динара Ркх, Дильназ Ахмадиева, Макпал Исабекова и многие другие.</p>
              </div>
              
<br>
               
            
          </div>
        </div>
      </div>
    </section><section class="section-padding--large bg-soapstone" style="margin-top: -20%;">
    <div class="container">
      <div class="section-intro pb-70px">
        <h2 class="section-intro__title">Как выиграть поездку?</h2>
        <h2 class="section-intro__subtitle">Путевка На остров <span class="d-block">Хайнань</span></h2>
      </div>

      <div class="row mb-xl-5 pb-4">
        <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
          <div class="card card-team">
            <div class="card-team__img">
            </div>
            <div class="card-team__position">
              <div class="card-footer d-flex justify-content-between align-items-center">
                <div class="card-team__bio">
                  <h4><a href="#">Шаг 1 - Регистрация</a></h4>
                  <p>Зарегистрируйтесь,<br>укажите свои данные и пройдите регистрацию.<br><br><br></p>
                </div>
              
              </div> <a href="register.php" class="button button-hero" id="t1" ">Регистрация</a>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
          <div class="card card-team">
            <div class="card-team__img">
            </div>
            <div class="card-team__position">
              <div class="card-footer d-flex justify-content-between align-items-center">
                <div class="card-team__bio">
                  <h4><a href="#">Шаг 2 - Покупка Курса</a></h4>
                  <p>Произведите оплату курса, удобным для вас способом.На вашу почту придет подтверждение оплаты с чеком.Если возник вопрос с оплатой, обратитесь в нашу тех.поддержку.</p>
                </div>
              </div>               <a href="/pays/pay.php?cost=30000" class="button button-hero" id="t2" ">Купить</a>

            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 mb-4 mb-lg-0" id="d3">
          <div class="card card-team" id="c1">
            <div class="card-team__img">
            </div>
            <div class="card-team__position">
              <div class="card-footer d-flex justify-content-between align-items-center">
                <div class="card-team__bio">
                  <h4><a href="#">Шаг 3 - Обучение</a></h4>
                  <p>Пройдите все 5 видеокурсов и получите именной сертификат о прохождении.<br><br></p>
                </div>
               
              </div>               <a href="blog.php" class="button button-hero" id="t3" >Видеокурсы</a>

            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
        <div class="card card-team" id="card1">
            <div class="card-team__img">
            </div>
            <div class="card-team__position">
              <div class="card-footer d-flex justify-content-between align-items-center">
                <div class="card-team__bio">
                  <h4><a href="#">Шаг 4 - Поделиться Успехом</a></h4>
                  <p>Опубликуйте сертификат, распечатайте сертификат и выложите его у себя в Instagram stories с хештегом #atamasonline2019</p>
                </div>
              </div>                <a href="/pays/pay.php?cost=30000" class="button button-hero" id="t4">Купить</a>

            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
          
        </div>
        <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
          
      </div>
    </div>
  </section> 



  </main>


  <!-- ================ start footer Area ================= -->
  <footer class="footer-area section-gap">
		<div class="container">
			<div class="row">
				<div class="col-xl-2 col-sm-6 mb-4 mb-xl-0 single-footer-widget">
					<h4>Помощь</h4>
					<ul>
          <li><a href="privacy_policy.php">Политика конфиденциальности</a></li>
						<li><a href="public_offer.php">Публичная оферта</a></li>
					</ul>
				</div>
			



			</div>
			<div class="footer-bottom row align-items-center text-center text-lg-left">
				<p class="footer-text m-0 col-lg-8 col-md-12"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> Все права защищены авторским правом | Сделано  в <a href="https://saico.agency" target="_blank">SAICO 28</a>
<!-- 	<div class="col-lg-4 col-md-12 text-center text-lg-right footer-social">
					<a href="#"><i class="fab fa-facebook-f"></i></a>
					<a href="#"><i class="fab fa-twitter"></i></a>
					<a href="#"><i class="fab fa-dribbble"></i></a>
					<a href="#"><i class="fab fa-behance"></i></a>
				</div> --></p>
			
			</div>
		</div>
	</footer>
  <!-- ================ End footer Area ================= -->

  <script src="vendors/jquery/jquery-3.2.1.min.js"></script>
  <script src="vendors/bootstrap/bootstrap.bundle.min.js"></script>
  <script src="vendors/sticky/jquery.sticky.js"></script>
  <script src="js/jquery.ajaxchimp.min.js"></script>
  <script src="vendors/Magnific-Popup/jquery.magnific-popup.min.js"></script>
</body>
</html>
