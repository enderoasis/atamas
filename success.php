<?php
	require_once 'vendor/autoload.php';
	require_once 'db.php';
	use Paybox\Pay\Facade as Paybox;
	$paybox = new Paybox();
	$paybox->merchant->id = getenv('PG_MERCHANT_ID');
	$paybox->merchant->secretKey = getenv('PG_SEKRET_KEY');
	$request = (array_key_exists('pg_xml', $_REQUEST))
    	? $paybox->parseXML($_REQUEST)
    	: $_REQUEST;
    extract($request);
    $order_id = $pg_order_id;    

    $order = R::load('orders', $pg_order_id);
	$order->status = 1;
	$order->pg_payment_id = $pg_payment_id; 
	R::store($order);

	require 'paymentSuccess.php';
?>