<?php
  session_start();

  require 'db.php';

	$data1 = $_POST;

  $user_email = $_SESSION['email'];

  $user = R::findOne('users', ' email = ? ', array($user_email));
  $order = R::findLast('orders', ' user_id = ? ', array($user->id));



 	if (empty($_SESSION['logged_user'])) {
    header( 'Refresh: 0; url=login.php' );
  }

  elseif ($order->status == NULL) {
    header( 'Refresh: 0; url=purchase.php' );
  }
  else {

  }

  //elseif (!isset( $_SESSION['haspaid'])) {
    // header( 'Refresh: 0; url=check.php' );
  // }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Видеокурсы</title>
		<link rel="icon" href="https://partners.saico.pro/favicon.png">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" integrity="sha256-0rguYS0qgS6L4qVzANq4kjxPLtvnp5nn2nB5G1lWRv4=" crossorigin="anonymous"></script>
        <script src="vendors/jquery/jquery-3.2.1.min.js"></script>
  <link rel="stylesheet" href="vendors/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="vendors/fontawesome/css/all.min.css">
  <link rel="stylesheet" href="vendors/themify-icons/themify-icons.css">
  <link rel="stylesheet" href="vendors/flat-icon/flaticon.css">
	<link rel="stylesheet" href="vendors/nice-select/nice-select.css">
  <link rel="stylesheet" href="vendors/Magnific-Popup/magnific-popup.css">
  <link rel="stylesheet" href="vendors/OwlCarousel/owl.theme.default.min.css">
  <link rel="stylesheet" href="vendors/OwlCarousel/owl.carousel.min.css">

  <link rel="stylesheet" href="css/style.css">
</head>
<body class="blog-bg">
  <!-- ================ start header Area ================= -->
	<header class="header_area sticky-header">
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light main_box">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					 aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
						<ul class="nav navbar-nav menu_nav ml-auto">
<li class="nav-item"><a class="nav-link" href="index.php">Главная</a></li>
							<li class="nav-item"><a class="nav-link" href="index.php#about">О курсе</a></li>

 <li class="nav-item"><a class="nav-link" href="public_offer.php">Публичная оферта</a></li>


                                <!--<li class="nav-item active"><a class="nav-link" href="#gallery">Галерея</a></li>-->

							<li class="nav-item"><a class="nav-link" href="contact.html">Контакты</a></li>
     <?php if (!isset($_SESSION['reg']) && !isset($_SESSION['logged_user'] )) { ?>         <li class="nav-item"><a class="nav-link" href="login.php">Вход</a></li>  <?php } ?>



						</ul>
					</div>
				</div>
			</nav>
		</div>
	</header>
  <!-- ================ end header Area ================= -->




  <!--================Blog Categorie Area =================-->

  <!--================Blog Categorie Area =================-->

  <!--================Blog Area =================-->
  <section class="blog_area">

  	<br><br><br><br><br><br>
      <div class="container">
          <div class="row">
              <div class="col-lg-8">
                  <div class="blog_left_sidebar">
                      <article class="row blog_item">
                          <div class="col-md-3">
                              <div class="blog_info text-right">

                                  <ul class="blog_meta list">
                                      <li>
                                          <a href="#"> 
                                              <i class="lnr lnr-user"></i>
                                          </a>
                                      </li>
                                      <li>
                                          <a href="#"> 
                                              <i class="lnr lnr-calendar-full"></i>
                                          </a>
                                      </li>

                                  </ul>
                              </div>
                          </div>
                          <div class="col-md-9">
                              <div class="blog_post">
                              <div style="position:relative; overflow:hidden; padding-bottom:56.25%" id="v1"> <iframe src="https://cdn.jwplayer.com/players/If68iBqY-5HCh7NEL.html" width="100%" height="100%" frameborder="0" scrolling="auto" title="Main 1 1" style="position:absolute;" allowfullscreen></iframe> </div> <div class="blog_details">

                                          <h2>Wedding Angel / Свадебный образ</h2>

                                      <p></p>
                                  </div>
                              </div>
                          </div>
                      </article>
                      <br><br><br>
                      <article class="row blog_item">
                          <div class="col-md-3">
                              <div class="blog_info text-right">

                                  <ul class="blog_meta list">
                                      <li>
                                          <a href="#"> 
                                              <i class="lnr lnr-user"></i>
                                          </a>
                                      </li>
                                      <li>
                                          <a href="#"> 
                                              <i class="lnr lnr-calendar-full"></i>
                                          </a>
                                      </li>

                                  </ul>
                              </div>
                          </div>
                          <div class="col-md-9">
                              <div class="blog_post">
                              <div style="position:relative; overflow:hidden; padding-bottom:56.25%" id="v2"> <iframe src="https://cdn.jwplayer.com/players/Dg11sDVD-5HCh7NEL.html" width="100%" height="100%" frameborder="0" scrolling="auto" title="Main 2" style="position:absolute;" allowfullscreen></iframe> </div> <div class="blog_details">

                                          <h2>Victoria Secret / Нежный девичьий макияж</h2>

                                      <p></p>
                                  </div>
                              </div>
                          </div>
                      </article><br><br><br>
                      <article class="row blog_item">
                          <div class="col-md-3">
                              <div class="blog_info text-right">

                                  <ul class="blog_meta list">
                                      <li>
                                          <a href="#"> 
                                              <i class="lnr lnr-user"></i>
                                          </a>
                                      </li>
                                      <li>
                                          <a href="#"> 
                                              <i class="lnr lnr-calendar-full"></i>
                                          </a>
                                      </li>

                                  </ul>
                              </div>
                          </div>
                          <div class="col-md-9">
                              <div class="blog_post">
                              <div style="position:relative; overflow:hidden; padding-bottom:56.25%" id="v3"> <iframe src="https://cdn.jwplayer.com/players/iEv1cOAu-5HCh7NEL.html" width="100%" height="100%" frameborder="0" scrolling="auto" title="Main 3" style="position:absolute;" allowfullscreen></iframe> </div>
                                  <div class="blog_details">

                                          <h2>Hollywood Glam/Образ Кинодивы</h2>
                                      <p></p>
                                  </div>
                              </div>
                          </div>
                      </article><br><br><br>
                      <article class="row blog_item">
                          <div class="col-md-3">
                              <div class="blog_info text-right">

                                  <ul class="blog_meta list">
                                      <li>
                                          <a href="#"> 
                                              <i class="lnr lnr-user"></i>
                                          </a>
                                      </li>
                                      <li>
                                          <a href="#"> 
                                              <i class="lnr lnr-calendar-full"></i>
                                          </a>
                                      </li>

                                  </ul>
                              </div>
                          </div>
                          <div class="col-md-9">
                              <div class="blog_post">
                              <div style="position:relative; overflow:hidden; padding-bottom:56.25%" id="v4"> <iframe src="https://cdn.jwplayer.com/players/cZ679L5l-5HCh7NEL.html" width="100%" height="100%" frameborder="0" scrolling="auto" title="Main 4 1" style="position:absolute;" allowfullscreen></iframe> </div><div class="blog_details">

                                          <h2>Matte Drama/Пронзительный Глубокий Макияж</h2>

                                      <p></p>
                                  </div>
                              </div>
                          </div>
                      </article><br><br><br>
                      <article class="row blog_item">
                          <div class="col-md-3">
                              <div class="blog_info text-right">

                                <ul class="blog_meta list">
                                      <li>
                                          <a href="#"> 
                                              <i class="lnr lnr-user"></i>
                                          </a>
                                      </li>
                                      <li>
                                          <a href="#"> 
                                              <i class="lnr lnr-calendar-full"></i>
                                          </a>
                                      </li>

                                  </ul>
                              </div>
                          </div>
                          <div class="col-md-9">
                              <div class="blog_post">
                              <div style="position:relative; overflow:hidden; padding-bottom:56.25%" id="v5" onclick= "redir()"> <iframe src="https://cdn.jwplayer.com/players/SofGI6ai-5HCh7NEL.html" width="100%" height="100%" frameborder="0" scrolling="auto" title="Main 5" style="position:absolute;" allowfullscreen></iframe> </div><div class="blog_details">

                                          <h2>Fashion Obsession / Буйство Акцентов</h2>

                                      <p></p>
                                      <a href="https://atamas.online/sertificate/" class="button button-hero" id="b2" >Получить сертификат</a>

                                  </div>
                              </div>
                          </div>
                     
                      </article>
                      <nav class="blog-pagination justify-content-center d-flex">
                          <ul class="pagination">
                            <!--  <li class="page-item">
                                  <a href="#" class="page-link" aria-label="Previous">
                                      <span aria-hidden="true">
                                          <span class="lnr lnr-chevron-left"></span>
                                      </span>
                                  </a>
                              </li>


                              <li class="page-item">
                                  <a href="#" class="page-link" aria-label="Next">
                                      <span aria-hidden="true">
                                          <span class="lnr lnr-chevron-right"></span>
                                      </span>
                                  </a>
                              </li> -->
                          </ul>
                      </nav>
                  </div>
              </div>
              <div class="col-lg-4">
                  <div class="blog_right_sidebar">

                      <aside class="single_sidebar_widget author_widget">
                          <img class="author_img rounded-circle" src="img/banner/image2.jpeg" alt="" >
                          <h4>Владимир Атамас</h4>
                          <p>Визажист</p>
                         <br>
                          <p>Неважно, новичок вы или опытный мастер, вы с лёгкостью овладеете профессиональными техниками макияжа. Именно для вас я подготовил 5 пленительных образов.

Всё просто, доступно и профессионально!
                          </p>
                          <div class="br"></div>
                      </aside>




                  </div>
              </div>
          </div>
      </div>
  </section>
  <!--================Blog Area =================-->



  <!-- ================ start footer Area ================= -->
  <footer class="footer-area section-gap">
		<div class="container">
			<div class="row">
				
				


			</div>
			<div class="footer-bottom row align-items-center text-center text-lg-left">
				<p class="footer-text m-0 col-lg-8 col-md-12"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
		Copyright &copy;<script>document.write(new Date().getFullYear());</script> Все права защищены авторским правом | Сделано  в <a href="https://saico.agency" target="_blank">SAICO 28</a>
		<!-- <div class="col-lg-4 col-md-12 text-center text-lg-right footer-social">
					<a href="#"><i class="fab fa-facebook-f"></i></a>
					<a href="#"><i class="fab fa-instagram"></i></a>
					<a href="#"><i class="fab fa-github"></i></a>
					<a href="#"><i class="fab fa-behance"></i></a>
				</div> --></p>
				
			</div>
		</div>
	</footer>
  <!-- ================ End footer Area ================= -->

  <script src="vendors/bootstrap/bootstrap.bundle.min.js"></script>
  <script src="vendors/OwlCarousel/owl.carousel.min.js"></script>
  <script src="vendors/sticky/jquery.sticky.js"></script>
  <script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="js/mail-script.js"></script>
  <script src="vendors/Magnific-Popup/jquery.magnific-popup.min.js"></script>
  <script src="js/main.js"></script>
</body>
</html>
