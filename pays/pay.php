<?php

	require_once '../vendor/autoload.php';

	use \RedBeanPHP\R as R;
	use Paybox\Pay\Facade as Paybox;

	$dotenv = \Dotenv\Dotenv::create( $_SERVER['DOCUMENT_ROOT'] );
	$dotenv->load();

	$paybox = new Paybox();

	
	session_start();

	if (empty($_SESSION['email'])) {
		header('Location:' . '../'.getenv("LOGIN_URL"));
	} else {
		$user_email = $_SESSION['email'];
		$cost = $_GET['cost'];

		R::setup( 'mysql:host='.getenv("DB_HOST").';dbname='.getenv("DB_NAME") , getenv("DB_USER"), getenv("DB_PASS"), false );

		$user = R::findOne('users', ' email = ? ', array($user_email));
		$user_phone = str_replace(array('+',' ', '(' , ')', '-'), '', $user->phone);
		//$phone = preg_replace('![^0-9]+!', '', $user->phone)
		//echo $user_phone;

		$order = R::dispense("orders");
		$order->user_id = $user->id;
		$order->amount = $cost;
		$order_id = R::store( $order );

		$merchant = $paybox->getMerchant();
		$merchant->setId(getenv('PG_MERCHANT_ID'));
		$merchant->setSecretKey(getenv('PG_SEKRET_KEY'));

		$order = $paybox->getOrder();
		$paybox->order->id = $order_id;
		$order->setAmount($cost);
		$order->setDescription('Мастерство визажа от Владимира Атамаса');
		$paybox->getCustomer()->setUserEmail($user_email);
		$paybox->customer->pg_user_phone = $user_phone;

		if($paybox->init()) {
			sleep(5);
		    header('Location:' . $paybox->redirectUrl);
		}
	}
