<?php
	//require 'db.php';
	require_once 'vendor/autoload.php';
	use \RedBeanPHP\R as R;

	$dotenv = \Dotenv\Dotenv::create( $_SERVER['DOCUMENT_ROOT'] );
	$dotenv->load();

	R::setup( 'mysql:host='.getenv("DB_HOST").';dbname='.getenv("DB_NAME") , getenv("DB_USER"), getenv("DB_PASS"), false );

	session_start();

	if (empty($_SESSION['email'])) {
		header('Location:' . '../'.getenv("LOGIN_URL"));
	} else {
		$user_email = $_SESSION['email'];

		$user = R::findOne('users', ' email = ? ', array($user_email));

		$order = R::findOne('orders', ' user_id = ? ', array($user->id));

		if ($order->status == 1) {
			header('Location: blog.php');
		}
		else {
			header( 'Refresh: 0; url=purchase.php' );
		}
	}

?>