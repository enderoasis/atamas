<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$title?></title>
  	<link rel="icon" href="https://partners.saico.pro/favicon.png">
    <script src="js/main.js"></script>
    <link rel="stylesheet" href="vendors/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="vendors/themify-icons/themify-icons.css">
    <link rel="stylesheet" href="vendors/flat-icon/flaticon.css">
    <link rel="stylesheet" href="vendors/nice-select/nice-select.css">
    <link rel="stylesheet" href="vendors/Magnific-Popup/magnific-popup.css">
    <link rel="stylesheet" href="vendors/OwlCarousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="vendors/OwlCarousel/owl.carousel.min.css">
    <!-- @import url("https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700"); -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>

<div class="jumbotron text-center">
  <h1 class="display-3">Оплата успешна!</h1>
  <img src="/img/home/scess.png"  alt="альтернативный текст" align="center">

  <br><br><br><br>
  <p class="lead"><strong>Ваша Оплата</strong> успешно подтверждена, вам предоставлен полный доступ к материалам курса.</p>
  <hr>
  <p>
    У вас проблемы? <a href="">Тех.Поддержка</a>
  </p>
  <p class="lead">
    <a class="btn btn-primary btn-sm" href="blog.php" role="button">Начать Обучение</a>
  </p>
</div> <footer class="footer-area section-gap">
		<div class="container">
			<div class="row">
				<
				<div class="col-xl-2 col-sm-6 mb-4 mb-xl-0 single-footer-widget">
				
				</div>



			</div>
			<div class="footer-bottom row align-items-center text-center text-lg-left">
				<p class="footer-text m-0 col-lg-8 col-md-12"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> Все права защищены авторским правом | Сделано  в <a href="https://saico.agency" target="_blank">SAICO 28</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
				<div class="col-lg-4 col-md-12 text-center text-lg-right footer-social">
					<a href="#"><i class="fab fa-facebook-f"></i></a>
					<a href="#"><i class="fab fa-twitter"></i></a>
					<a href="#"><i class="fab fa-dribbble"></i></a>
					<a href="#"><i class="fab fa-behance"></i></a>
				</div>
			</div>
		</div>
	</footer></body>
</html>