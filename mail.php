<form action="" method="post">
    <input type="submit" value="Отправить тестовое письмо" />
    <input type="hidden" name="button_pressed" value="1" />
</form>

<p>
Тестовое письмо будет отправлено на hello@atamas.online.
<p>

<?php

if(isset($_POST['button_pressed']))
{


$to      = 'hello@atamas.online';
$subject = 'Тема';
$message = '<html>
<head>
  <title>Пример письма</title>
</head>
<body>
Код скрипта взят из официальной <a href="https://www.php.net/manual/ru/function.mail.php" target="_blank">документации</a>. Киррилица в теме и тексте отображается, т к есть заголовок "Content-Type: text/html; charset=utf-8"
</body>
</html>';
$headers = 'From: info@atamas.online' . "\r\n" .
    'Reply-To: info@atamas.online' . "\r\n" .
    'X-Mailer: PHP/' . phpversion() . "\r\n" .
	'Content-Type: text/html; charset=utf-8';

mail($to, $subject, $message, $headers);

}

?>
