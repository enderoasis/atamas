<?php
  require 'vendor/autoload.php';
  $dotenv = Dotenv\Dotenv::create(__DIR__);
  $dotenv->load();
  $title = getenv('APP_NAME');
  session_start();

  $pc = $_SESSION['email'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title><?=$title?> - Покупка видеокурса</title>
<link rel="icon" href="https://partners.saico.pro/favicon.png">

  <link rel="stylesheet" href="vendors/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="vendors/fontawesome/css/all.min.css">
  <link rel="stylesheet" href="vendors/themify-icons/themify-icons.css">
  <link rel="stylesheet" href="vendors/flat-icon/flaticon.css">
	<link rel="stylesheet" href="vendors/nice-select/nice-select.css">
  <link rel="stylesheet" href="vendors/Magnific-Popup/magnific-popup.css">
  <link rel="stylesheet" href="vendors/OwlCarousel/owl.theme.default.min.css">
  <link rel="stylesheet" href="vendors/OwlCarousel/owl.carousel.min.css">

  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <!-- ================ start header Area ================= -->
	<header class="header_area sticky-header">
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light main_box">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->

					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					 aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
						<ul class="nav navbar-nav menu_nav ml-auto">
            <li class="nav-item"><a class="nav-link" href="index.php">Главная </a></li>
              <li class="nav-item"><a class="nav-link" href="public_offer.php">Публичная оферта</a></li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
	</header>
  <!-- ================ end header Area ================= -->

  <!-- ================ start banner area ================= -->
	<section class="banner-area pricing" id="pricing">
		<div class="container h-100">
			<div class="banner-area__content text-center">
        <h1>Покупка видеокурса</h1>
        <nav aria-label="breadcrumb" class="banner-breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">Оплата видеокурса</li>
          </ol>
        </nav>
			</div>
    </div>
	</section>
	<!-- ================ end banner area ================= -->


  <!-- ================ pricing section start ================= -->
  <section class="section-margin--large">
    <div class="container">
      <div class="section-intro pb-70px">
        <h4 class="section-intro__title">Список курсов</h4>
        <h2 class="section-intro__subtitle">Выберите курс для <span class="d-block">Покупки</span></h2>
			</div>
			<div class="row gutters-48">
				<div class="col-lg-4 col-md-6 mb-5 pb-xl-4">

          <div class="card text-center card-pricing border-style">
            <div class="card-pricing__header">
              <h3>Первый Курс</h3>
              <br>
              <p>Пять видеоуроков</p>
            </div>
            <div class="card-pricing__price">
              <h2>30000 тенге</h2>
            </div>
            <ul class="card-pricing__list">
              <li>Wedding Angel/Свадебный образ</li>
              <li>Victoria Secret/Нежный девичьий макияж</li>
              <li>Hollywood Glam/Образ Кинодивы</li>
              <li>Matte Drama/Пронзительный Глубокий Макияж</li>
              <li>Fashion Obsession/Буйство Акцентов</li>
            </ul>
            <div class="mb-5">
              
              <!-- <a class="button button-pricing" href="https://api.paybox.money/payment.php?pg_merchant_id=519722&pg_amount=20&pg_currency=KZT&pg_description=%D0%92%D0%B8%D0%B4%D0%B5%D0%BE%D0%BA%D1%83%D1%80%D1%81%D1%8B+%D0%BE%D1%82+%D0%92%D0%BB%D0%B0%D0%B4%D0%B8%D0%BC%D0%B8%D1%80%D0%B0+%D0%90%D1%82%D0%B0%D0%BC%D0%B0%D1%81%D0%B0&pg_salt=5S7OihGKgoyRfuyr&pg_language=ru&pg_sig=a7cb71e11369066f460dd81cb3982a74">Купить</a> -->
              <a href="pays/pay.php?cost=30000" class="button button-pricing">Купить</a>

            </div>
          </div>
				</div>
				<div class="col-lg-4 col-md-6 mb-5 pb-xl-4">

        </div>
        <div class="col-lg-4 col-md-6 mb-5 pb-xl-4">

        </div>

        <div class="col-lg-4 col-md-6 mb-5 pb-xl-4">

        </div>

        <div class="col-lg-4 col-md-6 mb-5 pb-xl-4">

        </div>

        <div class="col-lg-4 col-md-6 mb-5 pb-xl-4">

				</div>

      </div>
		</div>
  </section>
  <!-- ================ pricing section end ================= -->


  <!-- ================ start footer Area ================= -->
  <footer class="footer-area section-gap">
		<div class="container">
			<div class="row">


							<div class="info"></div>
						</form>
					</div>
				</div>
			</div>
			<div class="footer-bottom row align-items-center text-center text-lg-left">
				<p class="footer-text m-0 col-lg-8 col-md-12"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> Все права защищены | Сделано в <a href="https://saico.agency" target="_blank">SAICO 28</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
				<div class="col-lg-4 col-md-12 text-center text-lg-right footer-social">

          <a href="#"><i class="fab fa-instagram"></i></a>
          <a href="#"><i class="fab fa-whatsapp"></i></a>
        	<a href="#"><i class="fab fa-facebook-f"></i></a>
					<a href="#"><i class="fab fa-twitter"></i></a>
				</div>
			</div>
		</div>
	</footer>
  <!-- ================ End footer Area ================= -->

  <script src="vendors/jquery/jquery-3.2.1.min.js"></script>
  <script src="vendors/bootstrap/bootstrap.bundle.min.js"></script>
  <script src="vendors/OwlCarousel/owl.carousel.min.js"></script>
  <script src="vendors/sticky/jquery.sticky.js"></script>
  <script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="js/mail-script.js"></script>
  <script src="vendors/Magnific-Popup/jquery.magnific-popup.min.js"></script>
  <script src="js/main.js"></script>
</body>
</html>
