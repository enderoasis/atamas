<?php
  session_start();
  require 'db.php';
	$data1 = $_POST;
 	if (empty($_SESSION['logged_user'])) {
    header( 'Refresh: 0; url=login.php' );
  }
  else {
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Видеокурсы</title>
		<link rel="icon" href="https://partners.saico.pro/favicon.png">


  <link rel="stylesheet" href="vendors/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="vendors/fontawesome/css/all.min.css">
  <link rel="stylesheet" href="vendors/themify-icons/themify-icons.css">
  <link rel="stylesheet" href="vendors/flat-icon/flaticon.css">
	<link rel="stylesheet" href="vendors/nice-select/nice-select.css">
  <link rel="stylesheet" href="vendors/Magnific-Popup/magnific-popup.css">
  <link rel="stylesheet" href="vendors/OwlCarousel/owl.theme.default.min.css">
  <link rel="stylesheet" href="vendors/OwlCarousel/owl.carousel.min.css">

  <link rel="stylesheet" href="css/style.css">
</head>
<body class="blog-bg">
  <!-- ================ start header Area ================= -->
	<header class="header_area sticky-header">
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light main_box">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					 aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
						<ul class="nav navbar-nav menu_nav ml-auto">
<li class="nav-item"><a class="nav-link" href="index.php">Главная</a></li>
							<li class="nav-item"><a class="nav-link" href="index.php#about">О курсе</a></li>

 <li class="nav-item"><a class="nav-link" href="public_offer.php">Публичная оферта</a></li>


                                <!--<li class="nav-item active"><a class="nav-link" href="#gallery">Галерея</a></li>-->

							<li class="nav-item"><a class="nav-link" href="contact.html">Контакты</a></li>
     <?php if (!isset($_SESSION['reg']) && !isset($_SESSION['logged_user'] )) { ?>         <li class="nav-item"><a class="nav-link" href="login.php">Вход</a></li>  <?php } ?>



						</ul>
					</div>
				</div>
			</nav>
		</div>
	</header>
  <!-- ================ end header Area ================= -->




  <!--================Blog Categorie Area =================-->

  <!--================Blog Categorie Area =================-->

  <!--================Blog Area =================-->
  <section class="blog_area">

  	<br><br><br><br><br><br>
      <div class="container">
          <div class="row">
              <div class="col-lg-8">
                  <div class="blog_left_sidebar">
                      <article class="row blog_item">
                          <div class="col-md-3">
                              <div class="blog_info text-right">

                                  <ul class="blog_meta list">
                                      <li>
                                          <a href="#">Дата публикации
                                              <i class="lnr lnr-user"></i>
                                          </a>
                                      </li>
                                      <li>
                                          <a href="#">29 Сен, 2019
                                              <i class="lnr lnr-calendar-full"></i>
                                          </a>
                                      </li>

                                  </ul>
                              </div>
                          </div>
                          <div class="col-md-9">
                              <div class="blog_post">
                                <div itemscope itemtype="https://schema.org/VideoObject"><meta itemprop="uploadDate" content="Mon Oct 21 2019 17:52:01 GMT+0600 (Восточный Казахстан)"/><meta itemprop="name" content="Ralph Breaks The Internet.2018.web-dl.1080p.selezen"/><meta itemprop="duration" content="P0D" /><meta itemprop="thumbnailUrl" content="https://content.jwplatform.com/thumbs/o78eBcyu-640.jpg"/><meta itemprop="contentUrl" content="https://content.jwplatform.com/videos/o78eBcyu-Xg3HNYEW.mp4"/><div style="position:relative; overflow:hidden; padding-bottom:56.25%"> <iframe src="https://cdn.jwplayer.com/players/o78eBcyu-jPQzveUC.html" width="100%" height="100%" frameborder="0" scrolling="auto" title="Ralph Breaks The Internet.2018.web-dl.1080p.selezen" style="position:absolute;" allowfullscreen></iframe> </div></div>
                                 <div class="blog_details">

                                          <h2>Wedding Angel / Свадебный образ</h2>

                                      <p></p>
                                  </div>
                              </div>
                          </div>
                      </article>
                      <br><br><br>



                      <nav class="blog-pagination justify-content-center d-flex">
                          <ul class="pagination">
                            <!--  <li class="page-item">
                                  <a href="#" class="page-link" aria-label="Previous">
                                      <span aria-hidden="true">
                                          <span class="lnr lnr-chevron-left"></span>
                                      </span>
                                  </a>
                              </li>


                              <li class="page-item">
                                  <a href="#" class="page-link" aria-label="Next">
                                      <span aria-hidden="true">
                                          <span class="lnr lnr-chevron-right"></span>
                                      </span>
                                  </a>
                              </li> -->
                          </ul>
                      </nav>
                  </div>
              </div>
              <div class="col-lg-4">
                  <div class="blog_right_sidebar">

                      <aside class="single_sidebar_widget author_widget">
                          <img class="author_img rounded-circle" src="img/banner/image2.jpeg" alt="" >
                          <h4>Владимир Атамас</h4>
                          <p>Визажист</p>
                          <div class="social_icon">
                              <a href="#">
                                  <i class="fab fa-facebook-f"></i>
                              </a>
                              <a href="#">
                                  <i class="fab fa-twitter"></i>
                              </a>
                              <a href="#">
                                  <i class="fab fa-github"></i>
                              </a>
                              <a href="#">
                                <i class="fab fa-behance"></i>
                              </a>
                          </div>
                          <p>Неважно, новичок вы или опытный мастер, вы с лёгкостью овладеете профессиональными техниками макияжа. Именно для вас я подготовил 5 пленительных образов.

Всё просто, доступно и профессионально!
                          </p>
                          <div class="br"></div>
                      </aside>




                  </div>
              </div>
          </div>
      </div>
  </section>
  <!--================Blog Area =================-->



  <!-- ================ start footer Area ================= -->
  <footer class="footer-area section-gap">
		<div class="container">
			<div class="row">
				<div class="col-xl-2 col-sm-6 mb-4 mb-xl-0 single-footer-widget">
					<h4>Помощь</h4>
					<ul>
						<li><a href="#">Как купить видеокурс?</a></li>
						<li><a href="#">Проблемы с доступом</a></li>
						<li><a href="#">Проблемы с оплатой</a></li>
						<li><a href="#">Вопросы - Ответы</a></li>
					</ul>
				</div>
				<div class="col-xl-2 col-sm-6 mb-4 mb-xl-0 single-footer-widget">
					<ul>

		<br>


		<br>

						<li><a href="#">Политика конфиденциальности</a></li>
						<li><a href="public_offer.php">Публичная оферта</a></li>
					</ul>
				</div>



			</div>
			<div class="footer-bottom row align-items-center text-center text-lg-left">
				<p class="footer-text m-0 col-lg-8 col-md-12"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
		Copyright &copy;<script>document.write(new Date().getFullYear());</script> Все права защищены авторским правом | Сделано  в <a href="https://saico.agency" target="_blank">SAICO 28</a>
		<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
				<div class="col-lg-4 col-md-12 text-center text-lg-right footer-social">
					<a href="#"><i class="fab fa-facebook-f"></i></a>
					<a href="#"><i class="fab fa-twitter"></i></a>
					<a href="#"><i class="fab fa-dribbble"></i></a>
					<a href="#"><i class="fab fa-behance"></i></a>
				</div>
			</div>
		</div>
	</footer>
  <!-- ================ End footer Area ================= -->

  <script src="vendors/jquery/jquery-3.2.1.min.js"></script>
  <script src="vendors/bootstrap/bootstrap.bundle.min.js"></script>
  <script src="vendors/OwlCarousel/owl.carousel.min.js"></script>
  <script src="vendors/sticky/jquery.sticky.js"></script>
  <script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="js/mail-script.js"></script>
  <script src="vendors/Magnific-Popup/jquery.magnific-popup.min.js"></script>
  <script src="js/main.js"></script>
</body>
</html>
