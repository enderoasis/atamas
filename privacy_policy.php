<?php  ?>
<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Публичная офертв</title>
	<link rel="icon" href="https://partners.saico.pro/favicon.png">
<script src="js/main.js"></script>
  <link rel="stylesheet" href="vendors/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="vendors/fontawesome/css/all.min.css">
  <link rel="stylesheet" href="vendors/themify-icons/themify-icons.css">
  <link rel="stylesheet" href="vendors/flat-icon/flaticon.css">
	<link rel="stylesheet" href="vendors/nice-select/nice-select.css">
  <link rel="stylesheet" href="vendors/Magnific-Popup/magnific-popup.css">
  <link rel="stylesheet" href="vendors/OwlCarousel/owl.theme.default.min.css">
  <link rel="stylesheet" href="vendors/OwlCarousel/owl.carousel.min.css">
<!-- @import url("https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700"); -->
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  

  <!-- ================ start header Area ================= -->
	<header class="header_area sticky-header">
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light main_box">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					 aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
						<ul class="nav navbar-nav menu_nav ml-auto">

							<li class="nav-item"><a class="nav-link" href="index.php#about">О курсе</a></li>
							                  <li class="nav-item"><a class="nav-link" href="blog.php">Видеокурсы</a></li>



                                <!--<li class="nav-item active"><a class="nav-link" href="#gallery">Галерея</a></li>-->
                                <li class="nav-item"><a class="nav-link" href="index.php">Главная </a></li>
                  <li class="nav-item"><a class="nav-link" href="purchase.php">Приобрести курс </a></li>
     <?php if (!isset($_SESSION['reg']) && !isset($_SESSION['logged_user'] )) { ?>         <li class="nav-item"><a class="nav-link" href="login.php">Вход</a></li>  <?php } ?>
    


						</ul>
					</div>
				</div>
			</nav>
		</div>
	</header>
  <!-- ================ end header Area ================= -->

  <main class="site-main">

    <!-- ================ start hero banner Area ================= -->
   
    <!-- ================ team section end ================= -->

    <!-- ================ pricing section start ================= -->
    <section class="section-margin--large pricing-relative">
      <div class="container">
<p>ПОЛИТИКА КОНФИДЕНЦИАЛЬНОСТИ Политика конфиденциальности (далее - Политика) действует в отношении персональных данных, которые&nbsp;atamas.online (далее &ndash; Организация) может получить от пользователя при посещении последним сайта https://atamas.online&nbsp;(далее &ndash; Сайт), и использовании его сервисов, а также разъясняет каким образом будет осуществляться обработка и защита персональных данных пользователя. Организация осуществляет сбор и обработку персональных данных пользователя в следующих целях:  Идентификации личности пользователя;  Передачи персональных данных третьим лицам с целью получения информации о пользователе необходимой для рассмотрения Заявки пользователя заключения Договора; Осуществления взаиморасчетов с пользователем;  Проведение статистических и иных исследований на основании предоставленных данных; Организация принимает на себя обязательство по соблюдению требований законодательства Республики Казахстан относительно защиты персональных данных. Организация может осуществлять сбор и хранение следующей информации о пользователе:  Персональные данные пользователей Сайта;  Персональные данные лиц, подавших заявку (Заявителей). К персональным данным может быть отнесена информация: 1. Информация которую Организация получает при посещении пользователем страниц Сайта. Данная информация включает в себя IP-адрес, информация из cookie, информация о браузере пользователя (или иной программе, с помощью которой осуществляется доступ к Сайту), время доступа, адрес запрашиваемой страницы, а также доменное имя провайдера интернет-услуг пользователя. 2. Информация, которую предоставляет Заявитель о себе при регистрации на сайте для заполнения Заявки, Такая информация обозначена на Сайте и к ней относятся: фамилия, имя и отчество, адрес электронной почты, контактные телефоны, а также иные данные, которые автоматически передаются Организации в процессе использования сайта, что среди прочего включает отправленную и согласованную Оферту; 3. Другая информация о пользователе, предоставление которой необходимо для использования Сайта. Конфиденциальность: Организация соблюдает профессиональную этику и требования обеспечения конфиденциальности. Для Организации важна защита персональных данных, реализованы все необходимые способы защиты персональной информации пользователей. Передача персональных данных третьим лицам допускается только при условии сохранения конфиденциальности с целью получения Организацией информации необходимой для рассмотрения Заявки. Организация вправе передавать персональные данные третьим лицам при условии получения согласия от пользователя, которое выражается путем акцепта Политики конфиденциальности на Сайте либо в силу требований законодательства Республики Казахстан с соблюдением установленной процедуры. Работники Организации допускаются к обработке персональных данных на основании приказа Организации при условии принятия обязательств по сохранению конфиденциальности, с целью участия в обработке полученной электронной Заявки и оказания услуг пользователю. Меры защиты персональных данных: Организация принимает все необходимые правовые, организационные, технические меры для защиты персональных данных пользователей от неправомерного или случайного доступа, уничтожения, изменения, блокирования, копирования, распространения, а также от иных неправомерных действий с персональными данными пользователей. Под организационными и техническими мерами понимается:  Допуск к организации, обработке и защите персональных данных только уполномоченных лиц;  Ограничение состава ответственных работников, имеющих доступ к персональным данным;  Информирование работников о требованиях законодательства РК и нормативных документов Организации по обработке и защите персональных данных;  Обеспечение учета и хранения материальных носителей персональных данных и их обращения, исключающего хищение, подмену, несанкционированное копирование и уничтожение;  Определение рисков и угроз безопасности персональных данных при их обработке;  Разработка и обеспечение системы защиты персональных данных;  Резервное копирование персональных данных; Иные меры защиты. Согласие пользователя на сбор, обработку и хранение его персональных данных Пользователь Сайта, подтвердив согласие с Политикой конфиденциальности при заполнении Заявки, тем самым выражает свое согласие и предоставляет разрешение на сбор, обработку и хранение и передачу своих персональных данных в порядке, определенном законодательством РК и настоящей Политикой. В случае если пользователь не согласен с условиями настоящей Политики конфиденциальности, следует воздержаться от использования Сайта и передачи персональных данных. Согласие на обработку персональных данных может быть отозвано пользователем путем направления отзыва в письменной форме на электронный адрес info@saico.agency.</p>
      </div>
    </section>



  </main>


  <!-- ================ start footer Area ================= -->
  <footer class="footer-area section-gap">
		<div class="container">
			<div class="row">
				<div class="col-xl-2 col-sm-6 mb-4 mb-xl-0 single-footer-widget">
					<h4>Помощь</h4>
					<ul>
						<li><a href="#">Как купить видеокурс?</a></li>
						<li><a href="#">Проблемы с доступом</a></li>
						<li><a href="#">Проблемы с оплатой</a></li>
						<li><a href="#">Вопросы - Ответы</a></li>
					</ul>
				</div>
				<div class="col-xl-2 col-sm-6 mb-4 mb-xl-0 single-footer-widget">
					<ul>

<br>


<br>

						<li><a href="#">Политика конфиденциальности</a></li>
						<li><a href="#">Публичная оферта</a></li>
					</ul>
				</div>



			</div>
			<div class="footer-bottom row align-items-center text-center text-lg-left">
				<p class="footer-text m-0 col-lg-8 col-md-12"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> Все права защищены авторским правом | Сделано  в <a href="https://saico.agency" target="_blank">SAICO 28</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
				<div class="col-lg-4 col-md-12 text-center text-lg-right footer-social">
					<a href="#"><i class="fab fa-facebook-f"></i></a>
					<a href="#"><i class="fab fa-twitter"></i></a>
					<a href="#"><i class="fab fa-dribbble"></i></a>
					<a href="#"><i class="fab fa-behance"></i></a>
				</div>
			</div>
		</div>
	</footer>
  <!-- ================ End footer Area ================= -->

  <script src="vendors/jquery/jquery-3.2.1.min.js"></script>
  <script src="vendors/bootstrap/bootstrap.bundle.min.js"></script>
  <script src="vendors/OwlCarousel/owl.carousel.min.js"></script>
  <script src="vendors/sticky/jquery.sticky.js"></script>
  <script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="js/mail-script.js"></script>
  <script src="vendors/Magnific-Popup/jquery.magnific-popup.min.js"></script>
  <script src="js/main.js"></script>
</body>
</html>