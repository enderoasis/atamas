<?php
session_start();

require 'db.php';
include_once 'vendors/smsc/smsc_api.php';
    $data1 = $_POST;
    //если кликнули на button
    if ( isset($data1['do_signup']) )
    {
    // проверка формы на пустоту полей
        $errors = array();
        if ( trim($data1['name']) == '' )
        {
            $errors[] = 'Введите логин';
        }

        if ( $data1['phone'] == '' )
        {
            $errors[] = 'Введите телефон';
        }
        if ( $data1['email'] == '' )
        {
            $errors[] = 'Введите почту';
        }

        //проверка на существование одинакового логина
        //проверка на существование одинакового email
        if ( R::count('users', "email = ?", array($data1['email'])) > 0)
        {
            $errors[] = 'Пользователь с таким Email уже существует!';
        }

        if ( empty($errors) )
        {
		$length = 4;
	   	$chars = '0123456789';
   		$count = mb_strlen($chars);

    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }
            //ошибок нет, теперь регистрируем
            $_SESSION['reg'] = $data1['email'];
            $_SESSION['pc2'] = $data1['email'];
            $ref = $_SESSION['refid'];
            $user = R::dispense('users');
            $gender = $_POST['gen_user'];
            $user->name = $data1['name'];
            $user->surname = $data1['surname'];
            $user->email = $data1['email'];
            $user->phone = $data1['phone'];
            $user->gender = $gender;
            $user->referer = $ref;
			$user->password = password_hash($result, PASSWORD_DEFAULT);
            R::store($user);
            
            $email = $data1['email'];
            $phone = $data1['phone'];
            $url = "https://partners.saico.pro/nrfpp?&name=".$email."&email=".$email."&password=".$result."&referer=".$ref;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, false);
            $data = curl_exec($curl);
            curl_close($curl);
           
           

           // $site = 'https://atamas.online/';
            //$subject = 'Подтверждение регистрации Atamas.online';
            //$message = "Вы успешно зарегистрировались! Ваш логин:".$email." Ваш пароль:  ".$result." Ссылка на подтверждение регистрации ".$site;
            //$headers =  'From: info@atamas.online' . "\r\n";
            //'Reply-To: info@atamas.online' . "\r\n" .
              //  'X-Mailer: PHP/' . phpversion() . "\r\n" .
                //'Content-Type: text/html; charset=utf-8';

            //mail($to, $subject, $message, $headers);
            echo "<b>Регистрация успешна! На ваш номер телефона , были отправлены данные для входа.</b>";
            header("Content-Type: text/html; charset=UTF-8");
            header( 'Refresh: 0; url=login.php' );

            list($sms_id, $sms_cnt, $cost, $balance) = send_sms("$phone", "Ваш логин:".$email."\nВаш пароль:".$result , 1);






        }
    }
?>


<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <title>Регистрация</title>
    <link rel="icon" href="https://partners.saico.pro/favicon.png">
    <meta name="author" content="A.R.G." />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <link href="css/styles.min.css" rel="stylesheet" media="screen">
    <link href="css/sweet-alert.css" rel="stylesheet" media="screen">
    <link href="css/emoji.css" rel="stylesheet">

        <script type="text/javascript">
      var widgetId1;
      var widgetId2;
      var widgetId3;
      var onloadCallback = function() {
        widgetId1 = grecaptcha.render(document.getElementById('chl'), {
          'sitekey' : '6LdSi2QUAAAAAIIL9OBjERVUi81AYNyQHBzXbvR7'
        });
        widgetId2 = grecaptcha.render(document.getElementById('chr'), {
          'sitekey' : '6LdSi2QUAAAAAIIL9OBjERVUi81AYNyQHBzXbvR7'
        });
        widgetId3 = grecaptcha.render(document.getElementById('cl'), {
          'sitekey' : '6LdSi2QUAAAAAIIL9OBjERVUi81AYNyQHBzXbvR7'
        });
      };
    </script>

    <script src="js/vendor/sweet-alert.js"></script>
    <script src="js/vendor/call.js"></script>
    <!--noindex--><!--googleoff: index--><noscript><span>Включите поддержку JavaScript :)</span></noscript><!--googleon: index--><!--/noindex-->
</head>

<body class="is-preloader preloading parallax">

    <div id="preloader" data-spinner="spinner7" data-screenbg="#fff"></div>

    <div class="page-wrapper">

    <div style="display:none;">

    </div>
    <header class="navbar navbar-fullwidth">
        <div class="topbar">
            <div class="container">


                <div class="nav-toggle">
                    <span></span>
                </div>

                <div class="toolbar">

                    <nav class="main-navigation">
                        <ul class="menu">



                            <li class="m-none-s-n menu-item-has-children">
                                <a class="m-none-s">|</a>
                            </li>


<!--
                            <li class="menu-item-has-children">
                                <a href="price.php">Прайс</a>
                            </li>

                            <li class="menu-item-has-children" id="newsReload">
                                <a id="news" href="news.php">Новости</a>
                            </li>

                            <li class="menu-item-has-children" id="contactsReload">
                                <a id="contacts" href="contacts.php">Контакты</a>
                            </li>
-->
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </header>       <div class ="headfix"></div>

<section class="page-title">
    <div class="container"></div>
</section>

    <section class="container padding-bottom-3x">
            <h1 class="block-title text-center scrollReveal sr-bottom">Пожалуйста, для начала пройдите регистрацию.</h1>
            <hr class="scrollReveal sr-bottom">

            <div class="row padding-top padding-bottom-3x scrollReveal sr-scaleUp sr-delay-2 sr-ease-in-out-back">
                <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <form id="register" class="borddiscr text-center" style="padding: 0 40px 40px 40px;" action=""  method="post">

                        <div class="text-center auth-div">
                            <ul class="nav-tabs auth-ul" role="tablist">
                                <li class="auth-li" style="border-right: 1px solid #ececec;"><a class="auth-a" href="login.php">Войти</a></li>
                                <li class="auth-li"><a class="auth-a" href="register.php">Регистрация</a></li>
                                                        </ul>
                        </div>
                        <hr class="auth-hr">

                        <input type="hidden" name="action" class="form-control" value="register">
                        <div class="form-group">
                            <label class = "label lblorder text-left">Ваше имя:</label>
                            <input type="text" name="name" class="form-control" value="<?php echo @$data1['name']; ?>" placeholder="Имя" required>
                        </div>
                                                <div class="form-group">
                            <label class = "label lblorder text-left">Ваша Фамилия:</label>
                            <input type="text" name="surname" class="form-control" value="<?php echo @$data1['surname']; ?>" placeholder="Фамилия" required>
                        </div>
                        <div class="form-group">
                            <label class="label lblorder text-left">Ваш Пол:</label>
                            <select size="1" name="gen_user">
                                <option >Укажите Пол</option>
                                <option value="M">Мужской</option>
                                <option value="F">Женский</option>
                            </select>
                        <div class="form-group">
                            <label class = "label lblorder text-left">Укажите вашу почту:</label>
                            <input type="email" name="email" class="form-control" value="<?php echo @$data1['email']; ?>" placeholder="Почта" required>
                        </div>
                                                <div class="form-group">
                            <label class = "label lblorder text-left">Номер телефона:</label>
                            <input type="tel" id="tel" name="phone" class="form-control" value="<?php echo @$data1['phone']; ?>" required>
                        </div>
 <script>
window.addEventListener("DOMContentLoaded", function() {
function setCursorPosition(pos, elem) {
    elem.focus();
    if (elem.setSelectionRange) elem.setSelectionRange(pos, pos);
    else if (elem.createTextRange) {
        var range = elem.createTextRange();
        range.collapse(true);
        range.moveEnd("character", pos);
        range.moveStart("character", pos);
        range.select()
    }
}

function mask(event) {
    var matrix = "+7 ___ ___ ____",
        i = 0,
        def = matrix.replace(/\D/g, ""),
        val = this.value.replace(/\D/g, "");
    if (def.length >= val.length) val = def;
    this.value = matrix.replace(/./g, function(a) {
        return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? "" : a
    });
    if (event.type == "blur") {
        if (this.value.length == 2) this.value = ""
    } else setCursorPosition(this.value.length, this)
};
    var input = document.querySelector("#tel");
    input.addEventListener("input", mask, false);
    input.addEventListener("focus", mask, false);
    input.addEventListener("blur", mask, false);
});
  </script>
                        <div id="captcha" class="form-group" style="display:none">
                            <hr>
                            <label class = "label lblorder text-left">Подтвердите что вы не робот:</label>
                            <div class = "g-recaptcha" id="cl"></div>
                        </div>
                        <button type="submit" name="do_signup">Регистрация</button>
                        <hr>
                        <a href="login.php" class="text-sm no-border">Уже зарегистрированы?</a>
                    </form>
                </div>
            </div>
    </section>

    <div style="display:none;">

    </div>

    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content text-center">
                <!--noindex--><!--googleoff: index--><h4>Войти</h4><!--googleon: index--><!--/noindex-->
                <hr>


                <form id="loginModalForm" action="javascript:void(null);" onsubmit="signInModal();" method="post">
                    <input type="hidden" name="action" class="form-control" value="login">
                    <input type="text" name="login" class="form-control" placeholder="Логин" required>
                    <input type="password" name="password" class="form-control" placeholder="Пароль" required>
                    <div id="captchaModalL" class="form-group" style="display:none">
                        <label class = "label lblorder text-left">Подтвердите что вы не робот:</label>
                        <div class = "g-recaptcha" id="chl"></div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <button type="submit" name="submit">Войти</button>
                    </div>
                    <hr>
                    <div class="text-center">
                        <!--noindex--><!--googleoff: index--><span class="text-sm text-semibold"><a class="no-border" href="reset.php">Забыли пароль?</a></span><!--googleon: index--><!--/noindex-->
                    </div>
                </form>


            </div>
        </div>
    </div>

    <div class="modal fade" id="registerModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content text-center">
                <!--noindex--><!--googleoff: index--><h4>Регистрация</h4><!--googleon: index--><!--/noindex-->
                <hr>


                <form id="registerModalForm" action="javascript:void(null);" onsubmit="signUpModal();" method="post">
                    <input type="hidden" name="action" class="form-control" value="register">
                    <input type="text" name="login" class="form-control" placeholder="Логин" required>
                    <input type="email" name="mail" class="form-control" placeholder="Почта" required>
                    <input type="password" name="password" class="form-control" placeholder="Пароль" required>
                    <input type="password" name="re-password" class="form-control" placeholder="Повторите пароль" required>
                    <div id="captchaModalR" class="form-group" style="display:none">
                        <label class = "label lblorder text-left">Подтвердите что вы не робот:</label>
                        <div class = "g-recaptcha" id="chr"></div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <button type="submit" name="submit2">Регистрация</button>
                    </div>
                    <hr>
                    <div class="text-center">
                        <!--noindex--><!--googleoff: index--><span class="text-sm text-semibold"><a class="no-border" href="login.php">Уже зарегистрированы?</a></span><!--googleon: index--><!--/noindex-->
                    </div>
                </form>


            </div>
        </div>
    </div>

        </div>



        <script src="js/jquery.maskedinput.min.js"></script>
        <script src="https://education.saico.pro/js/vendor/jquery-2.1.4.min.js"></script>
        <script src="https://education.saico.pro/js/vendor/preloader.min.js"></script>
        <script src="https://education.saico.pro/js/vendor/bootstrap.min.js"></script>
        <script src="https://education.saico.pro/js/vendor/scrollreveal.min.js"></script>
    </body>
</html>
